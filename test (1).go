package main

/**
Given 2 trees, find if the sub tree exists in the larger tree.  For instance, this would match


subtree         larger tree

3                   50
                    /\
                   3  100

this would not

subtree         larger tree

3                   50
 \                  /\
  5                3  100



Create the structs to represent the tree, as well as the functions to find the result.  Please provide tests proving your solution works.  Please note the following conditions

1) The node data type will be an int
2) No duplicate nodes are allowed in the tree

*/

type TreeNode struct {
    Root *Leaf;
}
type Leaf {
    LeafNumber integer;
    Leafs[] *Leaf;
    Alignment string; //left or right
}

func GetAlignments() {
   Alignments:=make([]string, 3)
   Alignments[0]="CENTER" 
   Alignments[1]="LEFT"
   Alignments[2]="RIGHT"
   return Alignments
}
func GetSubAlignments() {
   Alignments:=make([]string,2)
   Alignments[0]="LEFT"
   Alignments[1]="RIGHT"
   return Alignments
}

func GetLayerLeaf(Leaf* Leaf,counter integer, idx integer, alignment string) {
   Alignments:=GetAlignments()
   ReturnValue := False
   Leafs:=Leaf.Leafs 
   for i:=0;i<len(Leafs);i++{
         if Leafs[i].Alignment==alignment&&counter==idx {
                return true
         }
         ReturnValue := GetLayerLeaf(Leafs[i], counter+1, idx, alignment)
         if ReturnValue {
            return true
         }
    }
   return false
}
// optimize for insufficient lengths
func GetLeafRows(Leaf* Leaf) {
    rows:=1
    for len(Leaf.Leafs)>0{ 
         rows+=1
    }
    return rows
}
                        
    
func SolveProblem(Superset* TreeNode, Subset* TreeNode) (error, boolean) {
      LenOfSuperSet = Rows(Superset)
      LenOfSubset = Rows(Subset)
      RootSuper := Superset.Root
      RootSub := Subset.Root
      if LenOfSubset > LenOfSuperset {
               return error, nil
      }
      currentIdx = 0
      Alignments:= GetAlignments()
      TipLeaf := GetLayerLeaf(RootSub, 0)
      for i:=0; i<len(Alignments); i++  {
           for j := 0; j < LenOfSuperSet; j ++ {
                leaf1 := GetLayerLeaf(RootSuper, 0, j, Alignments[i])
                if leaf1.LeafNumber == TipLeaf.LeafNumber {
                       if TrySolveProblem(leaf1, TipLeaf) {
                                return nil, true
                        }
                }
           }
       }
       return nil, false
}


func TrySolveProblem(SuperLeaf* Leaf, Subleaf* Leaf) {
    LenOfSuperLeaf = GetLeafRows(Superleaf)
    LenofSubLeaf = GetLeafRows(Subleaf)
    if LenOfSuperLeaf != LenofSubLeaf {
         return false;
     }
    ListIdx:=1
    Alignments := GetSubAlignments()
    for i:=0;i <len(Alignments); i++ {
            Match1 := GetLayerLeaf(Superleaf, 0, ListIdx, Alignments[i])
            Match2 := GetLayerLeaf(Subleaf, 0, ListIdx, Alignments[i])
            if not Match1.LeafNumber==Match2.LeafNumber {
                   // check all directions
                   return false;
             }
           ListIdx+=1
    }
}

           
           
    
    


func main() {
      supersetLeafs := Leaf{}
      supersetLeafs.LeafNumber = 50
      supersetLeafs.Alignment="CENTER"
      supersetChild1 := Leaf{}
      supersetChild1.LeafNumber = 3
      supersetChild1.Alignment="LEFT"
      
      supersetChild2 := Leaf{}
      supersetChild.LeafNumber = 100
      supersetChild.Alignment ="RIGHT"
      supersetChildList := make(Leaf[], 2)
      supersetChildList[0]=supersetChild1
      supersetChildList[1]=supersetChild2
      supersetLeafs.Leafs=supersetChildList

      superset := TreeNode{}
      superset.Root = supersetLeafs
      
      // subset examples
      subset1Leafs := Leaf{}
      subset1Leafs.LeafNumber := 3
      subset1Leafs.Alignment = "CENTER"
      subset1LeafsChildList := make(Leaf[], 0)
      subset1Leafs.Leafs = subsetLeafsChildList
      subset1 := TreeNode{}
      subset1.Root = subset1Leafs
      // second example
      subset2Leafs := Leaf{}
      subset2Leafs.LeafNumber := 3
      subset2Leafs.Alignment = "CENTER"
      subset2LeafsChildList := make(Leaf[], 1)
      subset2Child1 := Leaf{}
      subset2Child1.LeafNumber = 5
      subset2LeafsChildList[0]=subset2Child1
      subset2Leafs.Leafs=subset2LeafsChildList
      subset2 := TreeNode{}
      subset2.Root = subset2Leafs
      SolveProblem(superset, subset1) 
      SolveProblem(superset, subset2)
             
}
